# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class NovelCrawlItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    """ 
    列表页
    """
    title = scrapy.Field()
    author = scrapy.Field()
    new_chapter = scrapy.Field()
    simple_introductio =scrapy.Field()
    volume =scrapy.Field()
    chapter = scrapy.Field()
    novel_id = scrapy.Field()
    chapter_id = scrapy.Field()
    
class NovelCrawDatailsItem(scrapy.Item):
    """
    详情页
    """
    novel_id = scrapy.Field()
    datails_id = scrapy.Field()
    title = scrapy.Field()
    content =scrapy.Field()
    





